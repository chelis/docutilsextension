#!/usr/bin/env python

# $Id: rst2Json.py mcampo 
# Author: mcampo <marcelacampo@gmail.com>

"""
A minimal front end to the Docutils Publisher, producing Docutils JSON.  """

try:
    import locale
    locale.setlocale(locale.LC_ALL, '')
except:
    pass

from docutils.core import publish_cmdline, default_description


description = ('Generates Docutils-native XML from standalone '
               'reStructuredText sources.  ' + default_description)

publish_cmdline(writer_name='json', description=description)
