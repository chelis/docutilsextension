# Source and destination file names.
test_source = "jsonExampleTest.txt"
test_destination = "ExampleJson.json"

# Keyword parameters passed to publish_file.
reader_name = "standalone"
parser_name = "rst"
writer_name = "json"
