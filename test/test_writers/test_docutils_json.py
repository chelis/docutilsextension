#!/usr/bin/env python

"""
Test for docutils JSON writer.
"""

from StringIO import StringIO

from __init__ import DocutilsTestSupport # must be imported before docutils
import docutils
import docutils.core

# sample strings
# --------------

sourcecomplete = u"""================
My awesome title
================

This is some text, and then there's::

  a codeblock

and now i've got a listing with

* one 
* two
* three

items. """

sourcetitle = u"""================
My awesome title
================"""

sourcelist = u"""
and now i've got a listing with

* one 
* two
* three

items."""

bodycomplete = u"""{ "nodetype":"document"
"subnodes" : [{ "nodetype":"title"
"text":"My awesome title"
},{ "nodetype":"paragraph"
"text":"This is some text, and then there's:"
},{ "nodetype":"literal_block"
"text":"a codeblock"
},{ "nodetype":"paragraph"
"text":"and now i've got a listing with"
},{ "nodetype":"bullet_list"
"subnodes" : [{ "nodetype":"list_item"
"subnodes" : [{ "nodetype":"paragraph"
"text":"one"
}]},{ "nodetype":"list_item"
"subnodes" : [{ "nodetype":"paragraph"
"text":"two"
}]},{ "nodetype":"list_item"
"subnodes" : [{ "nodetype":"paragraph"
"text":"three"
}]}]},{ "nodetype":"paragraph"
"text":"items."
}]}"""


bodylist = u"""{ "nodetype":"document"
"subnodes" : [{ "nodetype":"paragraph"
"text":"and now i've got a listing with"
},{ "nodetype":"bullet_list"
"subnodes" : [{ "nodetype":"list_item"
"subnodes" : [{ "nodetype":"paragraph"
"text":"one"
}]},{ "nodetype":"list_item"
"subnodes" : [{ "nodetype":"paragraph"
"text":"two"
}]},{ "nodetype":"list_item"
"subnodes" : [{ "nodetype":"paragraph"
"text":"three"
}]}]},{ "nodetype":"paragraph"
"text":"items."
}]}"""

bodytitle = u"""{ "nodetype":"document"
"subnodes" : [{ "nodetype":"title"
"text":"My awesome title"
}]}"""

def publish_json(settings, source):
    return docutils.core.publish_string(source=source.encode('utf8'),
                                        reader_name='standalone',
                                        writer_name='docutils_json',
                                        settings_overrides=settings)

# XML Test Case
# -------------

class DocutilsJSONTestCase(DocutilsTestSupport.StandardTestCase):

    settings = {'input_encoding': 'utf8',
                'output_encoding': 'iso-8859-1',
                '_disable_config': True,
               }

    def test_title(self):
        expected = u''
        expected += bodytitle
        result = publish_json(self.settings, sourcetitle)
        self.assertEqual(result, expected.encode('latin1'))
    
    def test_list(self):
        expected = u''
        expected += bodylist
        result = publish_json(self.settings, sourcelist)
        self.assertEqual(result, expected.encode('latin1'))
        
    def test_complete(self):
        expected = u''
        expected += bodycomplete
        result = publish_json(self.settings, sourcecomplete)
        self.assertEqual(result, expected.encode('latin1'))

    
if __name__ == '__main__':
    import unittest
    unittest.main()
