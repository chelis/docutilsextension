
"""
Simple document tree Writer, writes Docutils JSON
"""

__docformat__ = 'reStructuredText'

import sys

import json
from StringIO import StringIO

import docutils
from docutils import frontend, writers, nodes


class RawJSONError(docutils.ApplicationError): pass


class Writer(writers.Writer):

    supported = ('json',)
    """Formats this writer supports."""

    settings_spec = (
        '"Docutils JSON" Writer Options',
        None,
        (('Generate JSON with newlines before and after tags.',
          ['--newlines'],
          {'action': 'store_true', 'validator': frontend.validate_boolean}),
         ('Generate JSON with indents and newlines.',
          ['--indents'],
          {'action': 'store_true', 'validator': frontend.validate_boolean}),))
        


    config_section = 'docutils_json writer'
    config_section_dependencies = ('writers',)

    output = None
    """Final translated form of `document`."""

    def __init__(self):
        writers.Writer.__init__(self)
        self.translator_class = JSONTranslator 
    def translate(self):
        self.visitor = visitor = self.translator_class(self.document)
        self.document.walkabout(visitor)
        self.output = ''.join(visitor.output)


class JSONTranslator(nodes.GenericNodeVisitor):
    def __init__(self, document):
        nodes.NodeVisitor.__init__(self, document)

        # Reporter
        self.warn = self.document.reporter.warning
        self.error = self.document.reporter.error

        # Output
        self.output = []

    # generic visit and depart methods
    # --------------------------------

    def default_visit(self, node):
        """Default node visit method."""
        self.output.append('{ "nodetype":"' + self.getNodeType(node) + '"\n'  )
        if not isinstance(node, nodes.TextElement):
            self.output.append('"subnodes" : [')

    def getNodeType(self, node):
        return node.__class__.__name__
            
    def default_departure(self, node):
        """Default node depart method."""
        if  not isinstance(node, nodes.TextElement):
            self.output.append(']')
            
        self.output.append('}')
        next = node.next_node(siblings=True,descend=False)
        if next:
            self.output.append(",")
         

    # specific visit and depart methods
    # ---------------------------------

    def visit_Text(self, node):
        self.writeTextNode(node)
            
    def writeTextNode(self, node):
        self.output.append( '"text":"' + str(node) + '"\n')

    def depart_Text(self, node):
        pass
    